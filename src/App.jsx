import { useState } from 'react'

import './App.css'

import Countries from './components/countries'

function App() {
  const [styles, setStyles] = useState({})

  const handlemodes=(e)=>{
 
    if(e.target.textContent==="Dark Mode"){
        e.target.textContent="Light Mode"
      
     setStyles({
      backgroundColor:"black",
      color:"white"
     })
    }else{
      e.target.textContent="Dark Mode"
      setStyles({})
    }
}

  return (
    <>
    <div className='container' style={styles}>
      
      <Countries onhandlemodes={handlemodes}/>
      </div>
    </>
  )
}

export default App
