import React, { useEffect, useState } from "react";
import { Card, CardHeader, CardBody} from "@chakra-ui/react";
import Countrydata from "../service/countrydata";
import Header from "./Header";
import Filters from "./filters";
function Countries(props) {
  const [countriesdata, setCountriesdata] = useState([]);
  const [loading, setLoading] = useState(true);
  const [searchvalue, setSearchvalue] = useState("");
  const [regionvalue, setRegionvalue] = useState("");
  const [sortingvalue, setsortingvalue] = useState("");
  const [subregionvalue, setSubregionvalue] = useState("");
  const [landlockvalue,setLandlocvalue]=useState("on");

  useEffect(() => {
    
    Countrydata().then((res) => {
      setLoading(false);
      setCountriesdata(res);
    });
    
  });

  const regions = [
    ...countriesdata.reduce((acc, cur) => {
      acc.add(cur.region);
      return acc;
    }, new Set()),
  ];

  

  const subregion = [
    ...countriesdata.reduce((acc, cur) => {
      if(cur.region===regionvalue ){
        if (cur.subregion !== undefined) {
          acc.add(cur.subregion);
        }
      }else if(regionvalue===""){
        if (cur.subregion !== undefined) {
          acc.add(cur.subregion);
        }
      }
    

      return acc;
    }, new Set()),
  ];
  

  const handlesearch = (e) => {
    setSearchvalue(e.target.value);
  };

  const handleregion = (e) => {
    setRegionvalue(e.target.value);
  };

  const handlesort = (e) => {
    setsortingvalue(e.target.value);
  };

  const handlesubregion = (e) => {
    setSubregionvalue(e.target.value);
  };
  const handlelandlock=(e)=>{
    
    if(e.target.value==="on"){
        e.target.value="off";
        setLandlocvalue("off");
    }else
    {
        e.target.value="on";
        setLandlocvalue("on");
    }
  }
  





  countriesdata.sort((a, b) => {
    return b[`${sortingvalue}`] - a[`${sortingvalue}`];
  });

  const filterconditions =
    searchvalue.length === 0
      ? regionvalue === ""
        ?subregionvalue===""?landlockvalue==="on"? countriesdata:countriesdata.filter((country)=>{
          return country.landlocked===true;
        }):landlockvalue==="on"? countriesdata.filter((country)=>{
          return country.subregion===subregionvalue;
      }):countriesdata.filter((country)=>{
        return country.landlocked===true;
      }).filter((country)=>{
        return country.subregion===subregionvalue;
    })
        :subregionvalue===""?landlockvalue==="on"? countriesdata.filter((country) => {
            return country.region.toLowerCase() === regionvalue.toLowerCase();
          }):countriesdata.filter((country)=>{
            return country.landlocked===true;
          }).filter((country) => {
            return country.region.toLowerCase() === regionvalue.toLowerCase();
          })
          :landlockvalue==="on"?countriesdata.filter((country)=>{
            return country.subregion===subregionvalue;
        }).filter((country) => {
            return country.region.toLowerCase() === regionvalue.toLowerCase();
          }):countriesdata.filter((country)=>{
            return country.landlocked===true;
          }).filter((country)=>{
            return country.subregion===subregionvalue;
        }).filter((country) => {
            return country.region.toLowerCase() === regionvalue.toLowerCase();
          })


      : regionvalue === ""
      ?subregionvalue===""?landlockvalue==="on"? countriesdata.filter((country) => {
          return country.name.common
            .toLowerCase()
            .includes(searchvalue.toLowerCase());
        }):countriesdata.filter((country)=>{
          return country.landlocked===true;
        }).filter((country) => {
          return country.name.common
            .toLowerCase()
            .includes(searchvalue.toLowerCase());
        })
        :landlockvalue==="on"? countriesdata.filter((country)=>{
            return country.subregion===subregionvalue;
        }).filter((country) => {
            return country.name.common
              .toLowerCase()
              .includes(searchvalue.toLowerCase());
          }):countriesdata.filter((country)=>{
            return country.landlocked===true;
          }).filter((country)=>{
            return country.subregion===subregionvalue;
        }).filter((country) => {
            return country.name.common
              .toLowerCase()
              .includes(searchvalue.toLowerCase());
          })
      :subregionvalue===""?landlockvalue==="on"? countriesdata
          .filter((country) => {
            return country.region.toLowerCase() === regionvalue.toLowerCase();
          })
          .filter((country) => {
            return country.name.common
              .toLowerCase()
              .includes(searchvalue.toLowerCase());
          }):countriesdata.filter((country)=>{
            return country.landlocked===true;
          }).filter((country) => {
            return country.region.toLowerCase() === regionvalue.toLowerCase();
          })
          .filter((country) => {
            return country.name.common
              .toLowerCase()
              .includes(searchvalue.toLowerCase());
          })
          :landlockvalue==="on"?countriesdata.filter((country)=>{
            return country.subregion===subregionvalue;
        }).filter((country) => {
            return country.region.toLowerCase() === regionvalue.toLowerCase();
          })
          .filter((country) => {
            return country.name.common
              .toLowerCase()
              .includes(searchvalue.toLowerCase());
          })
          :countriesdata.filter((country)=>{
            return country.landlocked===true;
          }).filter((country)=>{
            return country.subregion===subregionvalue;
        }).filter((country) => {
            return country.region.toLowerCase() === regionvalue.toLowerCase();
          })
          .filter((country) => {
            return country.name.common
              .toLowerCase()
              .includes(searchvalue.toLowerCase());
          });
  



  return (
    <>
      <Header  onhandlemodes={props.onhandlemodes}/>
      <Filters
        countryregions={regions}
        countrysubregion={subregion}
        onhandlesearch={handlesearch}
        onhandleregion={handleregion}
        onhandlesort={handlesort}
        onhandlesubregion={handlesubregion}
        onhandlelandlock={handlelandlock}
      />
    
      <div className="eachcountry">
        <div className="cards" >
        {countriesdata.length===0?<h1>{"content Loading"}</h1> :filterconditions.length===0?<h1>{"No Countries found"}</h1> :filterconditions.map((country) => {
          return (
            <Card key={country.cca3} className="card" style={{border:"1px solid white"}}>
              <img src={`${country.flags.png}`} alt="flag" />
              <CardHeader>
                <p id="country-name">{country.name.common}</p>
              </CardHeader>
              <CardBody className="cardbody">
                <div className="population">
                  <p id="population">Population:</p>
                  <p id="value">{country.population?country.population:"no data"}</p>
                </div>
                <div className="region">
                  <p id="regions">Region:</p>
                  <p id="value">{country.region?country.region:"no data"}</p>
                </div>
                <div className="capital">
                  <p id="capital">Capital:</p>
                  <p id="value">{country.capital ? country.capital[0] : "no data"}</p>
                </div>
              </CardBody>
              
            </Card>
          );
        })}
</div>
      </div>
    </>
  );
}

export default Countries;

