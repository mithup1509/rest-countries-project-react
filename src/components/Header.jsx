import React from 'react';

function Header(props) {
    return ( 
        <div className='header-data'>
<h1>Where in the world?</h1>
<h2 id='darkmode' onClick={props.onhandlemodes}>Dark Mode</h2>
</div>
     );
}

export default Header;