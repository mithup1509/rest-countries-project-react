import React from 'react';

function Filters(props) {

    return ( 
        <div className='filters'>
            <div className='search'>
                <input type="text"  placeholder='Search for a country...' onChange={props.onhandlesearch}/>
            </div>
            <div className='filter-region'>
            
           
                <select name="region-filter" id="region" onChange={props.onhandleregion}>
                <option key={"all"} value="">select region</option>
                {props.countryregions.map((country)=>{
                    
                return <option key={`${country}`} value={`${country}`}>{country}</option>
            })}
                </select>
            </div>
            <div className='sortby'>
        <select name="sorting" id="sort" onChange={props.onhandlesort}>
            <option value="">sortby</option>
            <option value="population">sort by population</option>
            <option value="area">sort by area</option>
        </select>
            </div>
            <div className='filter-subregion'>
                <select name="subregion" id="subregion" onChange={props.onhandlesubregion}>
                <option key={"all"} value="">Select sub-region</option>
                {props.countrysubregion.map((subregion)=>{
                
                    return <option value={`${subregion}`} key={`${subregion}`}>{subregion}</option>
                })}
                </select>
            </div>
            <div className='landlocked'>
                <label htmlFor='landlock'>land locked</label>
                <input type="checkbox" id='landlock' onChange={props.onhandlelandlock} />
            </div>
        </div>
     );
}

export default Filters;